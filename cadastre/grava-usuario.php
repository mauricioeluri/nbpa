<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        session_start();
        include "../arquivos/funcoes.php";
        ?>
    </head>
    <body>
        <?php
        $Nome = $_POST['nome'];
        $Email = $_POST['email'];
        $Tel = $_POST['tel'];
        $Cpf = $_POST['cpf'];
        $Senha = $_POST['senha'];
        $Nascimento = $_POST['nascimento'];
        $Sexo = $_POST['sexo'];



        $target_dir = "../arquivos/img/usuario/";
        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
        $ext = end((explode(".", $target_file))); # extra () to prevent notice
        $nomeArq = uniqid() . '.' . $ext;
        $target_dir .= $nomeArq;
// Checar se o arquivo de imagem e verdadeiro
        if (isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if ($check !== false) {
                //echo "<br>O arquivo é uma imagem - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "<br>O arquivo não é uma imagem.";
                $uploadOk = 0;
            }
        }
// Check if file already exists
//if (file_exists($target_file)) {
//    echo "<br>O arquivo já existe.";
//    $uploadOk = 0;
//}
// Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            //echo "<br>O tamanho do arquivo é muito grande.";
            $uploadOk = 0;
        }
// Allow certain file formats
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
            //echo "<br>Somente arquivos JPG, JPEG, PNG & GIF são permitidos.";
            $uploadOk = 0;
        }
// Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            //echo "<br>Seu arquivo não foi enviado.";
            $nomeArq = '';
// if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_dir)) {
                chmod($target_dir, 0777);
                //echo "<br>O arquivo " . basename($_FILES["fileToUpload"]["name"]) . " foi enviado com sucesso.<br>$nomeArq";
            } else {
                //echo "<br>Desculpe, houve um erro ao enviar o seu arquivo.";
            }
        }

        //echo "<img src='$nomeArq'style='width:100; height:100px; border-radius:10px'/>";





        $aviso = "Erro!!\\nPor favor verifique os seguintes campos no formulário:";
        $erro = false;
        if ($Nome == '') {
            $aviso .= "\\nNome";
            $erro = true;
        }
        if ($Email == '') {
            $aviso .= "\\nEmail";
            $erro = true;
        }
        if($Tel == '' || ctype_alpha($Tel)) {
            $aviso .= "\\nTel";
            $erro = true;
        }
        if($Cpf == '' || ctype_alpha($Cpf) ) {
        //if (preg_match("/^[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}$/", $Cpf) || $Cpf == ''){
            $aviso .= "\\nCpf";
            $erro = true;
        }
        if ($Senha == '') {
            $aviso .= "\\nSenha";
            $erro = true;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Nascimento)) {
            $aviso .= "\\nNascimento";
            $erro = true;
        }
        if ($Sexo != 'm' && $Sexo != 'f') {
            $aviso .= "\\nSexo";
            $erro = true;
        }

        if ($erro == true) {
            $aviso .= ".";
            echo $aviso;
            $_SESSION['usuario']['erro'] = $aviso;
            echo $_SESSION['usuario']['login'];
            header('location: ../index.php');
            exit();
        } else {
            $aviso = '';
        }

        $pdo = conecta();
        //$SenhaH = password_hash($Senha, PASSWORD_DEFAULT);
        $sql = $pdo->prepare("INSERT INTO cliente (nome, email, tel, cpf, senha,nascimento,sexo, perf_img) VALUES ('$Nome', '$Email', '$Tel', '$Cpf', '$Senha','$Nascimento','$Sexo', '$nomeArq') ");
        $sql->execute();

        $_SESSION['usuario']['login'] = $Email;
        $_SESSION['usuario']['usuario'] = $Nome;
        $_SESSION['usuario']['erro'] = '';
        
        header('location: ../index.php');
        ?>
    </body>
</html>
