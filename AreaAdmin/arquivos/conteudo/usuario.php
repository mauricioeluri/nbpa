<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        background: red;
        cursor: inherit;
        display: block;
    }
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>
<script>

    $(document).on('change', '.btn-file :file', function () {
        var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready(function () {
        $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log)
                    alert(log);
            }

        });
    });
</script>
<?php
$erro = $_SESSION['admin']['erro'];
if ($erro !== '') {
    echo "<script>alert('$erro');</script>";
    $_SESSION['admin']['erro'] = '';
}
?>
<h1>Usuário</h1>
<div id="clickMeId">
    <a class="btn btn-primary btn-large adicionar" onclick="show('formulario');
            hide('clickMeId')">
        Adicionar
    </a>
</div>
<div id="formulario" style="display:none;">

    <div class="container jumbotron">
        <div style="float: right;" id="clickMeId2" onclick="show('clickMeId');
                hide('formulario')">
            <i style="color:#981C19;" class="fa fa-times fa-2x"></i>
        </div>
        <h2 class="text-center">Cadastro de Usuário</h2>
        <div class="alert alert-danger"></div>
        <hr>
        <form class="form-horizontal" action="grava-usuario.php" method="POST" id="user" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Foto de usuário</label>
                <!-- <div class="col-sm-9"> -->
                    <!-- <input type="file" name="fileToUpload" > -->

                <div class="col-sm-9">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <span class="btn btn-primary btn-file">
                                Browse&hellip; <input name="fileToUpload" type="file" accept="image/*" >
                            </span>
                        </span>
                        <input type="text" class="form-control" readonly>
                    </div>
                </div>
                <!-- </div> -->
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-9">
                    <input
                        name="nome" type="text" id="nome" maxlength="30" class="form-control" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9">
                    <input name="email" type="email" id="email" maxlength="30" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Telefone</label>
                <div class="col-sm-9">
                    <input name="tel" type="text" id="tel" maxlength="18" onKeyDown="Mascara(this, Telefone);" onKeyPress="Mascara(this, Telefone);" onKeyUp="Mascara(this, Telefone);"  class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">CPF</label>
                <div class="col-sm-9">
                    <input maxlength="14" onKeyDown="Mascara(this, Cpf);" onKeyPress="Mascara(this, Cpf);" onKeyUp="Mascara(this, Cpf)
                                    ;" name="cpf" type="text" id="cpf" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Senha</label>
                <div class="col-sm-9">
                    <input name="senha" type="password" id="senha" maxlength="30" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nascimento</label>
                <div class="col-sm-9">
                    <input  name="nascimento" type="date" id="nascimento" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Sexo</label>
                <div class="col-sm-9">
                    <div class="input-group-addon" style="background-color:transparent;">
                        <input name="sexo" type="radio" id="f" value="f" checked/> <label for="f" style="margin-right:20px">Feminino</label>
                        <input name="sexo" type="radio" id="m" value="m"/> <label for="m" >Masculino</label>
                    </div>
                </div>
            </div>
            <div class='nav'>
                <button class='btn btn-danger' onclick="show('clickMeId');
                        hide('formulario')" type="button">Cancelar</button>
                <button type="submit" class="btn btn-success">Enviar</button>
            </div>
        </form>
    </div>

</div>



<script>
    function show(toBlock) {
        setDisplay(toBlock, 'block');
    }
    function hide(toNone) {
        setDisplay(toNone, 'none');
    }
    function setDisplay(target, str) {
        document.getElementById(target).style.display = str;
    }
    function apagar() {
        decisao = confirm("Você tem certeza que deseja apagar este usuário do sistema?");
        if (decisao) {
            alert("Usuário apagado com sucesso!");
        } else {
            event.preventDefault();
        }
    }
</script>


<?php
include "../funcoes.php";
$pdo = conecta();

$buscaemail = $pdo->prepare("SELECT id, nome, email,senha,tel,cpf,nascimento,sexo,perf_img FROM cliente order by id desc");
$buscaemail->execute();
?>
<?php
while ($linha = $buscaemail->fetch(PDO::FETCH_ASSOC)) {
    echo "<div class='panel panel-default jumbotron caixa'>
    <div class='panel-body'>
        <div class='col-sm-12'>
        <form action='modifica-usuario.php' method='POST'>
                <input type='hidden' name='id_usu' value='";
    echo $linha['id'];
    echo "'>
                    <button type='submit' class='btn btn-info editar'>
                    <i class='fa fa-pencil-square-o'></i>
                        Editar
                    </button>
            </form>

            <form action='apaga-usuario.php' method='POST'>
                <input type='hidden' name='id_usu' value='";
    echo $linha['id'];
    echo "'>
                <button type='submit' class='btn btn-danger excluir' onclick='apagar();'>
                    <i class='fa fa-times-circle-o fa-1x'></i>
                    Excluir
                </button>
            </form>";
    echo "
            <img src='";
    if ($linha['perf_img'] == '' || $linha['perf_img'] == NULL) {
        $perf = '../../arquivos/img/padrao.png';
    } else {
        $perf = '../../arquivos/img/usuario/' . $linha['perf_img'];
    }
    echo"$perf' class='col-sm-4 img-user'>

            <div class='col-sm-8'><h3 class='nome_user'>"; echo $linha['nome']; echo "</h3></div>
            <p class='dados_user2 dados_user1'>Email: "; echo $linha['email']; echo "</p>
            <p class='dados_user2 dados_user1'>Telefone: "; echo $linha['tel']; echo "</p>
            <p class='dados_user2 dados_user1'>Idade: ";
    $dob = $linha['nascimento'];
    echo ageCalculator($dob);
    echo " anos</p>
        </div>
        <div class='clearfix'></div>
        <hr>
        <div class='div_50' style='float:left'><p class='dados_user1'>Cpf: "; echo $linha['cpf']; echo "</p></div>
        <div class='div_dados_user2 div_50' style='float:left;'><p class='dados_user1'>Sexo: ";
    if ($linha['sexo'] == 'm') {
        $sexo = 'Masculino';
    } else {
        $sexo = 'Feminino';
    }
    echo " $sexo</p></div>
    </div>
</div>";
}
?>
<script type="text/javascript" >
    function Mascara(o, f) {
        v_obj = o;
        v_fun = f;
        setTimeout("execmascara()", 1);
    }
    //Função que Executa os objetos
    function execmascara() {
        v_obj.value = v_fun(v_obj.value);
    }
    //Função que padroniza telefone (11) 4184-1241
    function Telefone(v) {
        v = v.replace(/\D/g, "").replace(/^(\d\d)(\d)/g, "($1) $2").replace(/(\d{4})(\d)/, "$1-$2");
        return v;
    }
    //Função que padroniza CPF
    function Cpf(v) {
        v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
        return v;
    }

    $(document).ready(function () {

        // valida todos os campos ao deixar o foco deles
        $('#nome, #email, #tel, #cpf, #senha, #nascimento').blur(function () {
            validarCampo($(this));
        });

        /* VALIDAÇÃO ELEMENTOS */
        $('#user').submit(function (event) {

            var alerta = $('.alert');
            var alertaTexto = "";

            // valida o campo nome
            if ($('#nome').val() == "") {
                alertaTexto += "Campo nome deve ser preenchido.<br>";
            }

            // valida o campo email
            if ($('#email').val() == "") {
                alertaTexto += "Campo email deve ser preenchido.<br>";
            } else {
                var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                if (!regexEmail.test($('#email').val())) {
                    alertaTexto += "Campo email deve ser um email válido.<br>";
                }
            }

            // valida o campo telefone
            if ($('#tel').val() == "") {
                alertaTexto += "Campo telefone deve ser preenchido.<br>";
            }



            // valida o campo CPF
            if ($('#cpf').val() == "") {
                alertaTexto += "Campo CPF deve ser preenchido.<br>";
            }

            //valida senha
            if ($('#senha').val().length <= 6) {
                alertaTexto += "Senha tem que possuir obrigatoriamente mais de 6 digitos.<br>";
            }

            // valida o campo nascimento
            if ($('#nascimento').val() == "") {
                alertaTexto += "Campo nascimento deve ser preenchido.<br>";
            }

            // SE EXISTIR ERRO NA VALIDAÇÃO MOSTRA A MENSAGEM DE ERRO
            if (alertaTexto != "") {
                alerta.html(alertaTexto);
                alerta.show();
                event.preventDefault(); // previne o formulário de ser submetido
            } else {
                alert('Formulário OK!'); // validação ok, todos os campos estão preenchidos
            }

        });
    });
</script>