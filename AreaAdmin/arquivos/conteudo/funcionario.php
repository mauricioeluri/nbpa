<script src="../../arquivos/js/normal/form.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="../../arquivos/js/normal/pwstrength.js" type="text/javascript"></script>
<script src="../../arquivos/js/sweet-alert.min.js" type="text/javascript"></script>

<script>
<?php
$erro = $_SESSION['admin']['erro'];
if ($erro !== '') {
    echo "alert('$erro');";
    $_SESSION['admin']['erro'] = '';
}
?>
</script>
<h1>Funcionário</h1>
<div id="clickMeId">
    <a class="btn btn-primary btn-large adicionar" onclick="show('formulario');
            hide('clickMeId')">
        Adicionar
    </a>
</div>
<div id="formulario" style="display:none;">
    <div class="container jumbotron">
        <div style="float: right;" id="clickMeId2" onclick="show('clickMeId');
                hide('formulario')">
            <i style="color:#313131;" class="fa fa-times fa-2x"></i>
        </div>
        <h2 class="text-center">Cadastro de Funcionário<br><i class="fa fa-user-plus"></i></h2>
        <div class="alert alert-danger"></div>
        <hr style="margin-top: -23px;">
        <form class="form-horizontal" action="grava-funcionario.php" method="POST" id="cadastra">
            <div class="form-group has-feedback" id="nomeDiv">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-9">
                    <input id="nome" name="nome" type="text" maxlength="30" class="form-control" placeholder="Seu nome" autofocus>
                    <span class="fa fa-2x form-control-feedback" id="logoNome"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="emailDiv">
                <label class="col-sm-3 control-label">Email</label>
                <div class="col-sm-9 hint--top hint--rounded hint--always" data-hint='' id="mailmsg">
                    <input name="email" type="email" id="email" maxlength="40" class="form-control" placeholder="Seu email">
                    <span class="fa fa-2x form-control-feedback" id="logoMail"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="telDiv">
                <label class="col-sm-3 control-label">Telefone</label>
                <div class='col-sm-9 hint--top hint--rounded hint--always' data-hint='' id='telmsg'>
                    <input name="tel" type="text" id="tel" maxlength="18" class="form-control" placeholder="(53) 9999-9999">
                    <span class="fa fa-2x form-control-feedback" id="logoTel"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="cpfDiv">
                <label class="col-sm-3 control-label">CPF</label>
                <div class="col-sm-9 hint--top hint--rounded hint--always" data-hint='' id='cpfmsg'>
                    <input maxlength="14" name="cpf" type="text" id="cpf" class="form-control" placeholder="999.999.999-99">
                    <span class="fa fa-2x form-control-feedback" id="logoCpf"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="cepDiv">
                <label class="col-sm-3 control-label">CEP</label>
                <div class="col-sm-9 hint--top hint--rounded hint--always" data-hint='' id='cepmsg'>
                    <input maxlength="9" name="cep" type="text" id="cep" class="form-control" placeholder="99999-999">
                    <span class="fa fa-2x form-control-feedback" id="logoCep"></span>
                    <small><a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank" id="semcep" tabindex="-1">Não sei meu CEP</a></small>
                </div>
            </div>
            <div class="form-group has-feedback" id="senhaDiv">
                <label class="col-sm-3 control-label">Senha</label>
                <div class="col-sm-9 hint--top hint--rounded hint--always pwstrength_viewport_verdict" data-hint='' id='senhamsg'>
                    <input name="senha" type="password" id="senha" class="form-control" placeholder="Sua nova senha">
                    <input type="hidden" id="pwd" value="0"/>
                    <span class="fa fa-2x form-control-feedback" id="logoSenha"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="CfsenhaDiv">
                <label class="col-sm-3 control-label">Confirmar a senha</label>
                <div class="col-sm-9">
                    <input name="Cfsenha" type="password" id="Cfsenha" maxlength="30" class="form-control" placeholder="Repita sua nova senha">
                    <span class="fa fa-2x form-control-feedback" id="logoCfsenha"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="nascimentoDiv">
                <label class="col-sm-3 control-label">Nascimento</label>
                <div class="col-sm-9">
                    <input  name="nascimento" type="date" id="nascimento" class="form-control" placeholder="dd/mm/aaaa">
                    <span class="fa fa-2x form-control-feedback" id="logoNascimento"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="sexo">
                <label class="col-sm-3 control-label">Sexo</label>
                <div class="col-sm-9">
                    <div class="input-group-addon" style="background-color:transparent;" id="sb">
                        <input name="sexo" type="radio" id="f" value="f" /> <label for="f" style="margin-right:20px">Feminino</label>
                        <input name="sexo" type="radio" id="m" value="m"/> <label for="m" >Masculino</label>
                        <span class="fa fa-2x form-control-feedback" id="logoSexo"></span>
                    </div>
                </div>
            </div>
            <div class="form-group has-feedback" id="ruaDiv">
                <label for="inputPassword" class="col-sm-3 control-label">Rua</label>
                <div class="col-sm-9">
                    <input  name="rua" type="text" id="rua" class="form-control" size="60" readonly placeholder="Campo preenchido automaticamente..." tabindex="-1">
                    <span class="fa fa-2x form-control-feedback" id="logoRua"></span>
                </div>
            </div>
            <div class="form-group has-feedback" id="bairroDiv">
                <label for="inputPassword" class="col-sm-3 control-label">Bairro</label>
                <div class="col-sm-9">
                    <input name="bairro" type="text" id="bairro" class="form-control" size="60" readonly placeholder="Campo preenchido automaticamente..." tabindex="-1">
                    <span class="fa fa-2x form-control-feedback" id="logoBairro"></span>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Cidade</label>
                <div class="col-sm-9">
                    <input name="cidade" type="text" id="cidade" class="form-control" size="40" readonly placeholder="Campo preenchido automaticamente..." tabindex="-1">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword" class="col-sm-3 control-label">Estado</label>
                <div class="col-sm-9">
                    <input name="uf" type="text" id="uf" size="2" class="form-control" readonly placeholder="Campo preenchido automaticamente..." tabindex="-1">
                </div>
            </div>
            <div class="form-group">
                <br>
                <div class="col-sm-3"></div>
                <div class="col-sm-9 g-recaptcha" id='recaptcha' data-sitekey="6LebWxQTAAAAAFajbRfbEhngA_ekBQAqgY_Y735z"></div>
            </div>
            <div class='nav'>
                <button class='btn btn-danger' type="button" id='cancelar'>Cancelar</button>
                <button type="submit" class="btn btn-success" id="enviar" disabled>Enviar</button>
            </div>
        </form>
    </div>
</div>
<?php
include "../funcoes.php";
$pdo = conecta();
$buscafuncionario = $pdo->prepare("SELECT id, nome, email,tel,cpf,cep,senha,nascimento,sexo,rua,bairro,cidade,uf,perf_img FROM funcionario order by id desc");
$buscafuncionario->execute();
?>
<?php
while ($linha = $buscafuncionario->fetch(PDO::FETCH_ASSOC)) {
    echo "<div class='panel panel-default jumbotron caixa'>
    <div class='panel-body'>
        <div class='col-sm-12'>
        <form action='modifica-funcionario.php' method='POST'>
                <input type='hidden' name='id_func' value='";
    echo $linha['id'];
    echo "'>
                    <button type='submit' class='btn btn-info editar'>
                    <i class='fa fa-pencil-square-o'></i>
                        Editar
                    </button>
            </form>

            <form action='apaga-funcionario.php' method='POST'>
                <input type='hidden' name='id_func' value='";
    echo $linha['id'];
    echo "'>
                <button type='submit' class='btn btn-danger excluir'>
                    <i class='fa fa-times-circle-o fa-1x'></i>
                    Excluir
                </button>
            </form>";
    echo "
            <img src='";
    if ($linha['perf_img'] == '' || $linha['perf_img'] == NULL) {
        $perf = '../../arquivos/img/padrao.png';
    } else {
        $perf = '../../arquivos/img/funcionario/' . $linha['perf_img'];
    }
    echo"$perf' class='col-sm-4 img-user'>

            <div class='col-sm-8'><h3 class='nome_user'>";
    echo $linha['nome'];
    echo"</h3></div>
            <p class='dados_user2 dados_user1'>Email: ";
    echo $linha['email'];
    echo "</p>
            <p class='dados_user2 dados_user1'>Telefone: ";
    echo $linha['tel'];
    echo "</p>
            <p class='dados_user2 dados_user1'>Idade: ";
    $dob = $linha['nascimento'];
    echo ageCalculator($dob);
    echo " anos</p>
        </div>
        <div class='clearfix'></div>
        <hr>
        <div class='div_50' style='float:left'><p class='dados_user1'>Cpf: ";
    echo $linha['cpf'];
    echo "</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>Bairro: ";
    echo $linha['bairro'];
    echo "</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>Cidade: Bagé</p></div>
        <div class='div_50' style='float:left'><p class='dados_user1'>";
    echo $linha['rua'];
    echo "</p></div>
        <div class='div_dados_user2 div_50' style='float:left;'><p class='dados_user1'>Sexo: ";
    if ($linha['sexo'] == 'm') {
        $sexo = 'Masculino';
    } else {
        $sexo = 'Feminino';
    }
    echo " $sexo</p></div>
        <div class='div_50' style='float:right;'><p class='dados_user1'>Uf: ";
    echo $linha['uf'];
    echo "</p></div>
    </div>
</div>";
}
?>

<link href="../../arquivos/css/normal/form.css" rel="stylesheet" type="text/css"/>
<link href="../../arquivos/css/hint.min.css" rel="stylesheet" type="text/css"/>
<link href="../../arquivos/css/normal/sweet-alert.css" rel="stylesheet" type="text/css"/>