<?php

$erro = $_SESSION['admin']['erro'];
if ($erro !== '') {
    echo "<script>alert('$erro');</script>";
    $_SESSION['admin']['erro'] = '';
}
?>
<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        background: red;
        cursor: inherit;
        display: block;
    }
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>
<h1>Animais</h1>
<div id="clickMeId">
    <a class="btn btn-primary btn-large adicionar" onclick="show('formulario');
            hide('clickMeId')">
        Adicionar
    </a>
</div>

<div id="formulario" style="display:none;">
    <div class="container jumbotron">
        <div style="float: right;" id="clickMeId2" onclick="show('clickMeId');
                hide('formulario')">
            <i style="color:#981C19;" class="fa fa-times fa-2x"></i>
        </div>
        <h2 class="text-center">Cadastro de Animal</h2>
        <div class="alert alert-danger"></div>
        <hr>
        <form class="form-horizontal" method="POST" action="grava-animal.php" data-toggle="validator" id="contactForm" role="form" enctype="multipart/form-data">
            <div class="form-group">
                <label class="col-sm-3 control-label">Nome</label>
                <div class="col-sm-9">
                    <input name="nome" type="text" id="nome" maxlength="30" class="form-control" autofocus>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Genero</label>
                <div class="col-sm-9">
                    <div class="input-group-addon" style="background-color:transparent;">
                        <input name="genero" type="radio" id="f" value="f"/> <label for="f" style="margin-right:20px">Feminino</label>
                        <input name="genero" type="radio" id="m" value="m" checked/> <label for="m" >Masculino</label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Raça</label>
                <div class="col-sm-9">
                    <input name="raca" type="text" id="raca" maxlength="30" class="form-control">
                </div>                </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Pelo</label>
                <div class="col-sm-9">
                    <input name="pelo" type="text" id="pelo" maxlength="30" class="form-control">
                </div>                </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Nascimento</label>
                <div class="col-sm-9">
                    <input  name="nascimento" type="date" id="nascimento" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label">Previsão de Adoção</label>
                <div class="col-sm-9">
                    <input  name="previsao_adocao" type="date" id="previsao_adocao" class="form-control" data-error="Erro">
                </div>
            </div>
            <div class="form-group">
                <div class="btn btn-info" id="add">Adicionar imagem do animal</div>
                <input type="hidden" name="nro_img" id="nro_img">
            </div>
            <div id="imagens">

            </div>
            <hr>
            <div class="form-group">
                <textarea rows="4" style="width:400px;" name="descricao" id="descricao" placeholder="Informações Adicionais (opcional)." maxlength="240"></textarea>
            </div>


            <div class='nav'>
                <button class='btn btn-danger' onclick="show('clickMeId');
                        hide('formulario')" type="button" id="contactForm">Cancelar</button>

                <button type="submit" id="contactForm" class="btn btn-success">Enviar</button>
            </div>
        </form>
    </div>
</div>

<script>
    function show(toBlock) {
        setDisplay(toBlock, 'block');
    }
    function hide(toNone) {
        setDisplay(toNone, 'none');
    }
    function setDisplay(target, str) {
        document.getElementById(target).style.display = str;
    }

    function apagar() {
        decisao = confirm("Você tem certeza que deseja apagar este animal do sistema?");
        if (decisao) {
            alert("Animal apagado com sucesso!");
        } else {
            event.preventDefault();
        }
    }
    $(document).ready(function () {
        var alerta = $('.alert');
        var alertaTexto = '';
        //VALIDAÇÃO ELEMENTOS

        $('#contactForm').submit(function (event) {
            if ($('#nome').val() == "") {
                alertaTexto += "<br>Preencha o campo nome.";
            }
            if ($('#genero').val() == "") {
                alertaTexto += "<br>Preencha o campo gênero.";
            }
            if ($('#raca').val() == "") {
                alertaTexto += "<br>Preencha o campo raça.";
            }
            if ($('#pelo').val() == "") {
                alertaTexto += "<br>Preencha o campo pelo.";
            }
            if ($('#nascimento').val() == "") {
                alertaTexto += "<br>Preencha o campo nascimento.";
            }
            if ($('#previsao_adocao').val() == "") {
                alertaTexto += "<br>Preencha o campo previsão de adoção.";
            }
            if (alertaTexto != "") {
                alerta.html(alertaTexto);
                alerta.show();
                event.preventDefault(); // previne o formulário de ser submetido
                alertaTexto = '';
            } else {
                alert('Cadastro realizado com sucesso!'); // validação ok, todos os campos estão preenchidos
            }
        });


        //add fotos
        var n_foto = 0;
        var foto = 0;
        $('#add').click(function (event) {
            if (n_foto < 5) {
                $("#imagens").append("<div class='form-group'><label class='col-sm-3 control-label'>Foto do animal</label><div class='col-sm-9'><div class='input-group'><span class='input-group-btn'><span class='btn btn-primary btn-file'>Browse&hellip; <input name='fileToUpload" + foto++ + "' type='file' accept='image/*' ></span></span><input type='text' class='form-control' readonly></div></div></div>");
                n_foto++;
                $("#nro_img").val(n_foto);
            }
            $(document).on('change', '.btn-file :file', function () {
                var input = $(this),
                        numFiles = input.get(0).files ? input.get(0).files.length : 1,
                        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });

            $(document).ready(function () {
                $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

                    var input = $(this).parents('.input-group').find(':text'),
                            log = numFiles > 1 ? numFiles + ' files selected' : label;

                    if (input.length) {
                        input.val(log);
                    } else {
                        if (log)
                            alert(log);
                    }

                });
            });
        });

    });
</script>

<?php

include "../funcoes.php";

$pdo = conecta();
$buscaanimal = $pdo->prepare("SELECT id, nome, genero, raca, pelo, nascimento, previsao_adocao, descricao, img1, img2, img3, img4, img5 FROM animal order by id desc;");
$buscaanimal->execute();
while ($linha = $buscaanimal->fetch(PDO::FETCH_ASSOC)) {
    echo "
    <div class='container jumbotron'>
        <h2 class='nome_user'>";
    echo $linha['nome'];
    echo"</h2>
        <hr>

      <div class='form-group col-sm-12'>

      <form action='modifica-animal.php' method='POST'>
      <input type='hidden' name='id_anim' value='";
    echo $linha['id'];
    echo "'>
      <button type='submit' class='btn btn-info editar'>
      <i class='fa fa-pencil-square-o'></i>
      Editar
      </button>
      </form>
      <form action='apaga-animal.php' method='POST'>
      <input type='hidden' name='id_anim' value='";
    echo $linha['id'];
    echo"'>
      <button type='submit' class='btn btn-danger excluir' onclick='apagar();'>
      <i class='fa fa-times-circle-o fa-1x'></i>
      Excluir
      </button>
      </form>
      <div>";
    $x = 1;
    //$linha['img'.$x];
    while ($x < 5) {
        if ($linha['img' . $x] !== '' && $linha['img' . $x] !== NULL) {
            echo "<div class='col-sm-12' style='margin-top:10px; margin-bottom:20px;'><img src='../../arquivos/img/animal/";
            echo $linha['img' . $x];
            echo"' style='width:100%;'></div>";
        }
        $x++;
    }
    echo "</div>

      <form class='form-horizontal' action='grava-pedido.php' method='POST'>
      <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Idade: ";
    $dob = $linha['nascimento'];
    echo ageCalculator($dob);
    echo " anos</p>
      </div>
      <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Raça: ";
    echo $linha['raca'];
    echo "</p></div>
      <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Gênero: ";
    if ($linha['genero'] == 'm') {
        $sexo = 'Masculino';
    } else {
        $sexo = 'Feminino';
    }
    echo "$sexo </p>
      </div>
      <div style='float:left'><p class='dados_user2 dados_user1'>Pelo: ";
    echo $linha['pelo'];
    echo "</p></div>
      </form>
      </div>
      <hr>
      <p class='dados_user2 dados_user1'>Descrição: ";
    if ($linha['descricao'] == '') {
        $desc = "sem descrição.";
    } else {
        $desc = $linha['descricao'];
    }
    echo "$desc</p>
      <hr>
      </div>";
}