<h1>Adoções</h1>





<script>
    function apagar() {
        decisao = confirm("Você tem certeza que deseja apagar esta adoção do sistema?");
        if (decisao) {
            alert("Adoção apagada com sucesso!");
        } else {
            event.preventDefault();
        }
    }
</script>


<?php
include "../funcoes.php";
$pdo = conecta();
$buscaadocoes = $pdo->prepare("select adocao.id as id_adocao,
	date_format(data_suposta, '%d-%m-%Y') as data_suposta,
	c.id as id_cliente,
	c.nome as clie_nome,
	c.tel as clie_tel,
	c.email as clie_email,
	a.id as ani_id,
	a.nome as ani_nome,
    date_format(previsao_adocao, '%d-%m-%Y') as previsao_adocao,
	a.descricao as anim_descri,
    date_format(data_confirm, '%d-%m-%Y') as data_confirm
	from adocao
 inner join cliente c on (adocao.id_clie = c.id)
 inner join animal a on (adocao.id_anim = a.id)
   group by adocao.id,
	    data_suposta,
	    c.id,
	    c.nome,
	    c.tel,
	    c.email,
	    a.id,
	    a.nome,
	    a.previsao_adocao,
	    a.descricao,
	    data_confirm
	    order by adocao.id desc;");
$buscaadocoes->execute();
?>
<table border="1" cellpadding = 13;>
    <caption class="tp">Dados Cadastrados</caption>
    <thead  bgcolor = "#0099FF">
    <th>Data Suposta</th>
    <th>Nome Cliente</th>
    <th>Telefone Cliente</th>
    <th>Email Cliente</th>
    <th>Nome Animal</th>
    <th>Previsão Adoção</th>
    <th>Descrição</th>
    <th>Data Confirmada</th>


</thead>

<?php
$z = 0;
$cor1 = '#fff';
$cor2 = '#ddd';


while ($linha = $buscaadocoes->fetch(PDO::FETCH_ASSOC)) {
    echo "<tr bgcolor='";
    if ($z == 0) {
        echo $cor1;
        $z++;
    } else {
        echo $cor2;
        $z = 0;
    }
    ?> '>
    <form action="confirma-adocao.php" method="POST">
        <input type="hidden" name="id" value="<?= $linha['id_adocao'] ?>"/>
        <input type="hidden" name="id_clie" value="<?= $linha['id_cliente'] ?>"/>
        <input type="hidden" name="id_anim" value="<?= $linha['ani_id'] ?>"/>
        <input type="hidden" name="data_supo" value="<?= $linha['data_suposta'] ?>"/>
        <td><?= $linha['data_suposta'] ?></td>

        <td><?= $linha['clie_nome'] ?></td>
        <td><?= $linha['clie_tel'] ?></td>
        <td><?= $linha['clie_email'] ?></td>
        <td><?= $linha['ani_nome'] ?></td>
        <td><?= $linha['previsao_adocao'] ?></td>
        <td><?= $linha['anim_descri'] ?></td>
        <td><?php
            if ($linha['data_confirm'] == null) {
                echo "<button type='submit' class='btn btn-success center-block'>Confirmar!</button>";
            } else {
                echo $linha['data_confirm'];
            }
            ?></td>
    </form>
    </tr>
    <?php
}
?>
</table>