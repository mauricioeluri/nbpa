<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <script src="../arquivos/js/jquery-2.1.4.min.js" type="text/javascript"></script>
        <script src="../arquivos/js/bootstrap.min.js" type="text/javascript"></script>
        <link href="../arquivos/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <title>Login - Área Admin</title>
        <style>
            @import url(http://fonts.googleapis.com/css?family=Roboto);

            /****** LOGIN MODAL ******/
            body {
                background-image: url("../arquivos/img/fundoANR.jpg")
            } .loginmodal-container {
                padding: 30px;
                max-width: 350px;
                width: 100% !important;
                background-color: #F7F7F7;
                margin: 0 auto;
                border-radius: 2px;
                box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
                overflow: hidden;
                font-family: roboto;
            }

            .loginmodal-container h1 {
                text-align: center;
                font-size: 1.8em;
                font-family: roboto;
            }

            .loginmodal-container input[type=submit] {
                width: 100%;
                display: block;
                margin-bottom: 10px;
                position: relative;
            }

            .loginmodal-container input[type=text], input[type=password] {
                height: 44px;
                font-size: 16px;
                width: 100%;
                margin-bottom: 10px;
                -webkit-appearance: none;
                background: #fff;
                border: 1px solid #d9d9d9;
                border-top: 1px solid #c0c0c0;
                /* border-radius: 2px; */
                padding: 0 8px;
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }

            .loginmodal-container input[type=text]:hover, input[type=password]:hover {
                border: 1px solid #b9b9b9;
                border-top: 1px solid #a0a0a0;
                -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
                -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
                box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
            }

            .loginmodal {
                text-align: center;
                font-size: 14px;
                font-family: 'Arial', sans-serif;
                font-weight: 700;
                height: 36px;
                padding: 0 8px;
                /* border-radius: 3px; */
                /* -webkit-user-select: none;
                  user-select: none; */
            }

            .loginmodal-submit {
                /* border: 1px solid #3079ed; */
                border: 0px;
                color: #fff;
                text-shadow: 0 1px rgba(0,0,0,0.1); 
                background-color: #4d90fe;
                padding: 17px 0px;
                font-family: roboto;
                font-size: 14px;
                /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
            }

            .loginmodal-submit:hover {
                /* border: 1px solid #2f5bb7; */
                border: 0px;
                text-shadow: 0 1px rgba(0,0,0,0.3);
                background-color: #357ae8;
                /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
            }

            .loginmodal-container a {
                text-decoration: none;
                color: #666;
                font-weight: 400;
                text-align: center;
                display: inline-block;
                opacity: 0.6;
                transition: opacity ease 0.5s;
            } 

            .login-help{
                font-size: 12px;
            }
            .escuro {
                position:fixed;
                height: 100%;
                width: 100%;
                background-color: rgba(0, 0, 0, 0.76);
                z-index: 9000;
            } .env_mail {
                position:fixed;
                width: 383px;
                height: 250px;
                background-color: rgba(0, 0, 0, 0.81);
                z-index: 9001;
                margin: 10% auto;
                left: 0;
                right: 0;
                padding-top: 5px;
                border-radius: 13px;
                padding: 20px;
            } .texto_senha{
                line-height: 21px;
                text-align: left;
                margin-top: 15px;
                color: beige;
            } .hr1{
                margin-top: 10px;
                margin-bottom: 18px;
            } .p2 {
                margin-top: 17px;
                font-size: 19px;
                line-height: 0px !important;
                float: left;
                text-decoration: underline;
                margin-left: 11px;
                color: beige;
            }
        </style>
        <script>
            function show(toBlock) {
                setDisplay(toBlock, 'block');
            }
            function hide(toNone) {
                setDisplay(toNone, 'none');
            }
            function setDisplay(target, str) {
                document.getElementById(target).style.display = str;
            }

        </script>
    </head>
    <body>
        <div id="esqueci" style="display:none;">
            <div class="escuro" onclick="hide('esqueci');"></div>
            <form action="../enviar.php" method="POST">
                <div class="jumbotron env_mail">
                    <h3 style="color: white;">Esqueceu a senha?</h3>
                    <hr style=" margin: 0;">
                    <p class="texto_senha">Escreva o endereço de e-mail da sua conta abaixo:</p>
                    <input type="text" class="form-control" style="font-size: 18px" name="email" autofocus/>
                    <hr class="hr1">

                    <p class="p2" onclick="hide('esqueci');" >cancelar</p>
                    <button type='submit' class='btn btn-success center-block' name='value' id='value' style="float: right">Enviar e-mail!</button>
                </div>
            </form>
        </div>
        <div class="modal-dialog">
            <div class="loginmodal-container">
                <h1>Autenticação Necessária</h1><br>
                <form method='post' action='login.php' accept-charset='UTF-8'>
                    <input type="text" id="email" name="email" placeholder="E-mail" autofocus>
                    <!--<input type="password" id="senha" name="senha" placeholder="Senha"> -->
                    <input style='margin-bottom: 15px;' type='password' placeholder='Senha' name='senha'>
                    <input class='btn btn-primary btn-block' type='submit' id='sign-in' value='Sign In'>
                </form>

                <div class="login-help" onclick="show('esqueci');">Esqueci a senha</div>
            </div>
        </div>

    </body>
</html>
