<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        //if (!isset($_POST)) {
        //    echo 'nao tem post';
        //    //header('Location: http://www.google.com/');
        // }
        if (!isset($_REQUEST['id_func'])) {
            header('Location: ../funcionario/');
        }
//        if (!isset($_SESSION['admin'])) {
        //          header('location: ../home');
        //        exit();
        //  } else {
        $id_func = $_REQUEST['id_func'];
        include '../arquivos/estrutura/header.php';

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>

        <script src="../../arquivos/js/normal/form.js" type="text/javascript"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="../../arquivos/js/normal/pwstrength.js" type="text/javascript"></script>
        <script src="../../arquivos/js/sweet-alert.min.js" type="text/javascript"></script>
    </head>
    <body>
        <?php
        $pdo = conecta();


        $sql = $pdo->prepare("Select * FROM funcionario WHERE id = '$id_func'");

        $sql->execute();




        while ($linha = $sql->fetch(PDO::FETCH_ASSOC)) {

            echo "
        <div class='container jumbotron'>
        <input type=hidden id='modifica' value='1'/>
            <h2 class='text-center'>Editar cadastro de Funcionário<br><i class='fa fa-user-plus'></i></h2>
            <div class='alert alert-danger'></div>
            <hr style='margin-top: -23px;'>
            <form class='form-horizontal' action='att-funcionario.php' method='POST' id='cadastra'>
                <div class='form-group has-feedback' id='nomeDiv'>
                    <label class='col-sm-3 control-label'>Nome</label>
                    <div class='col-sm-9'>
                        <input id='nome' name='nome' type='text' maxlength='30' class='form-control' placeholder='Seu nome' value='"; echo $linha['nome']; echo "' autofocus>
                        <span class='fa fa-2x form-control-feedback' id='logoNome'></span>
                    </div>
                </div>
                <div class='form-group has-feedback' id='emailDiv'>
                    <label class='col-sm-3 control-label'>Email</label>
                    <div class='col-sm-9 hint--top hint--rounded hint--always' data-hint='' id='mailmsg'>
                        <input name='email' type='email' id='email' maxlength='40' class='form-control'  placeholder='Seu email' value='"; echo $linha['email']; echo "'>
                        <span class='fa fa-2x form-control-feedback' id='logoMail'></span>
                    </div>
                </div>
                <div class='form-group has-feedback'  id='telDiv'>
                    <label class='col-sm-3 control-label'>Telefone</label>
                    <div class='col-sm-9 hint--top hint--rounded hint--always' data-hint='' id='telmsg' />
                    <input name='tel' type='text' id='tel' maxlength='18' class='form-control' placeholder='(53) 9999-9999' value='"; echo $linha['tel']; echo "'>
                        <span class='fa fa-2x form-control-feedback' id='logoTel'></span>
                    </div>
                </div>
                <div class='form-group has-feedback'  id='cpfDiv'>
                    <label class='col-sm-3 control-label'>CPF</label>
                    <div class='col-sm-9 hint--top hint--rounded hint--always' data-hint='' id='cpfmsg'>
                    <input maxlength='14' name='cpf' type='text' id='cpf' class='form-control' placeholder='999.999.999-99' value='"; echo $linha['cpf']; echo "'>
                        <span class='fa fa-2x form-control-feedback' id='logoCpf'></span>
                    </div>
                </div>
                <div class='form-group has-feedback' id='cepDiv'>
                    <label class='col-sm-3 control-label'>Cep</label>
                    <div class='col-sm-9 hint--top hint--rounded hint--always' data-hint='' id='cepmsg'>
                    <input maxlength='9' name='cep' type='text' id='cep' class='form-control' placeholder='99999-999' value='"; echo $linha['cep']; echo "'>
                    <span class='fa fa-2x form-control-feedback' id='logoCep'></span>
                    <small><a href='http://www.buscacep.correios.com.br/sistemas/buscacep/' target='_blank' id='semcep' tabindex='-1'>Não sei meu CEP</a></small>
                </div>
                </div>
                <div class='form-group has-feedback' id='senhaDiv'>
                    <label class='col-sm-3 control-label'>Senha</label>
                    <div class='col-sm-9 hint--top hint--rounded hint--always pwstrength_viewport_verdict' data-hint='' id='senhamsg'>
                        <div class='btn btn-info' id='alt_se'>Alterar Senha</div>
                        <input name='senha' type='password' id='senha' class='form-control' placeholder='Alterar senha' style='display:none;'>
                        <input type='hidden' id='pwd' value='0'/>
                        <span class='fa fa-2x form-control-feedback' id='logoSenha'></span>
                    </div>
                </div>
                <div class='form-group has-feedback' id='CfsenhaDiv' style='display:none'>
                    <label class='col-sm-3 control-label'>Confirmar a senha</label>
                    <div class='col-sm-9'>
                        <input name='Cfsenha' type='password' id='Cfsenha' maxlength='30' class='form-control' placeholder='Repita a nova senha'>
                        <span class='fa fa-2x form-control-feedback' id='logoCfsenha'></span>
                    </div>
                </div>
                <div class='form-group has-feedback' id='nascimentoDiv'>
                    <label class='col-sm-3 control-label'>Nascimento</label>
                    <div class='col-sm-9'>
                        <input name='nascimento' type='date' id='nascimento' class='form-control' placeholder='dd/mm/aaaa' value='"; echo $linha['nascimento']; echo "'>
                        <span class='fa fa-2x form-control-feedback' id='logoNascimento'></span>
                    </div>
                </div>
                <div class='form-group has-feedback' id='sexo'>
                    <label class='col-sm-3 control-label'>Sexo</label>
                    <div class='col-sm-9'>
                        <div class='input-group-addon' style='background-color:transparent;' id='sb'>";
            if ($linha['sexo'] == 'f') {
                $masc = "";
                $femin = "checked";
            } else {
                $masc = "checked";
                $femin = "";
            }
            echo
            "<input name='sexo' class='sexo' type='radio' id='f' value='f' $femin> <label for='f' style='margin-right:20px'>Feminino</label>
                            <input name='sexo' class='sexo' type='radio' id='m' value='m' $masc/> <label for='m' >Masculino</label>
                            <span class='fa fa-2x form-control-feedback' id='logoSexo'></span>
                        </div>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Rua</label>
                    <div class='col-sm-9'>
                        <input  name='rua' type='text' id='rua' class='form-control' size='60' readonly placeholder='Campo preenchido automaticamente...' value='"; echo $linha['rua']; echo "' tabindex='-1'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Bairro</label>
                    <div class='col-sm-9'>
                        <input name='bairro' type='text' id='bairro' class='form-control' size='60' readonly placeholder='Campo preenchido automaticamente...' value='"; echo $linha['bairro']; echo "' tabindex='-1'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Cidade</label>
                    <div class='col-sm-9'>
                        <input name='cidade' type='text' id='cidade' class='form-control' size='40' readonly placeholder='Campo preenchido automaticamente...' value='"; echo $linha['cidade']; echo "' tabindex='-1'>
                    </div>
                </div>
                <div class='form-group'>
                    <label for='inputPassword' class='col-sm-3 control-label'>Estado</label>
                    <div class='col-sm-9'>
                        <input name='uf' type='text' id='uf' size='2' class='form-control' readonly placeholder='Campo preenchido automaticamente...' value='"; echo $linha['uf']; echo "' tabindex='-1'>
                    </div>
                </div>
                <input type = 'hidden' name = 'id_func' value = '$id_func'>
                <div class='nav'>
                    <button type='submit' class='btn btn-danger' onclick='javascript:history.back()'>Cancelar</button>
                    <button type='submit' class='btn btn-success' id='enviar'>Enviar</button>
                </div>
            </form>
        </div>
        ";
        }
        include '../arquivos/estrutura/footer.php';
        ?>
        <link href="../../arquivos/css/normal/form.css" rel="stylesheet" type="text/css"/>
        <link href="../../arquivos/css/hint.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../arquivos/css/normal/sweet-alert.css" rel="stylesheet" type="text/css"/>