<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        session_start();
        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>
    </head>
    <body>
        <?php
        $Id = ($_REQUEST['id_func']);

        $Nome = $_POST['nome'];
        $Email = $_POST['email'];
        $Tel = $_POST['tel'];
        $Cpf = $_POST['cpf'];
        $Cep = $_POST['cep'];
        $Senha = $_POST['senha'];
        $Nascimento = $_POST['nascimento'];
        $Sexo = $_POST['sexo'];
        $Rua = $_POST['rua'];
        $Bairro = $_POST['bairro'];
        $Cidade = $_POST['cidade'];
        $Uf = $_POST['uf'];




        $aviso = "Erro!!\\nPor favor verifique os seguintes campos no formulário:";
        $erro = false;

        if ($Nome == '') {
            $aviso .= "\\nNome";
            $erro = true;
        }
        if ($Email == '') {
            $aviso .= "\\nEmail";
            $erro = true;
        }
        if ($Tel == '' || ctype_alpha($Tel)) {
            $aviso .= "\\nTelefone";
            $erro = true;
        }
        if ($Cpf == '') {
            $aviso .= "\\nCpf";
            $erro = true;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Nascimento)) {
            $aviso .= "\\nNascimento";
            $erro = true;
        }
        if ($Cep == '') {
            $aviso .= "\\nCep";
            $erro = true;
        }
        if ($Senha > 0 && $Senha < 7) {
            $aviso .= "\\nCep";
            $erro = true;
        }
        if ($Sexo != 'm' && $Sexo != 'f') {
            $aviso .= "\\nSexo";
            $erro = true;
        }
        if ($Rua == '') {
            $aviso .= "\\nRua";
            $erro = true;
        }
        if ($Bairro == '') {
            $aviso .= "\\nBairro";
            $erro = true;
        }
        if ($Cidade == '') {
            $aviso .= "\\nCidade";
            $erro = true;
        }
        if ($Uf == '') {
            $aviso .= "\\nUf";
            $erro = true;
        }
        if ($erro == true) {
            $aviso .= ".";
            echo $aviso;
            $_SESSION['admin']['erro'] = $aviso;
            header('location: index.php');
            exit();
        } else {
            $aviso = '';
        }

        $pdo = conecta();



        if ($Senha == '') {
            $sql = $pdo->prepare("UPDATE funcionario SET nome='$Nome', email='$Email', tel='$Tel', cpf='$Cpf', cep='$Cep', nascimento='$Nascimento', sexo='$Sexo', rua='$Rua', bairro='$Bairro', cidade='$Cidade', uf='$Uf' WHERE id=$Id");
        } else {
            //$SenhaH = password_hash($Senha, PASSWORD_DEFAULT);
            $sql = $pdo->prepare("UPDATE funcionario SET nome='$Nome', email='$Email', tel='$Tel', cpf='$Cpf', cep='$Cep', senha='$Senha', nascimento='$Nascimento', sexo='$Sexo', rua='$Rua', bairro='$Bairro', cidade='$Cidade', uf='$Uf' WHERE id=$Id");
        }

        $sql->execute();

        if (($_SESSION['admin']['email']) == $Email) {
            $_SESSION['admin']['usuario'] = $Nome;
        }

        header('location: index.php');
        ?>
    </body>
</html>
