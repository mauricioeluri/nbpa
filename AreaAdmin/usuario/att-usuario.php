<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        session_start();

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }

        ?>
    </head>
    <body>
        <?php
        $Id = ($_REQUEST['id_usu']);

        $Nome = $_POST['nome'];
        $Email = $_POST['email'];
        $Senha = $_POST['senha'];
        $Tel = $_POST['tel'];
        $Cpf = $_POST['cpf'];
        $Nascimento = $_POST['nascimento'];
        $Sexo = $_POST['sexo'];


        $aviso = "Erro!!\\nPor favor verifique os seguintes campos no formulário:";
        $erro = false;
        if ($Nome == '') {
            $aviso .= "\\nNome";
            $erro = true;
        }
        if ($Email == '') {
            $aviso .= "\\nEmail";
            $erro = true;
        }
        if ($Tel == '' || ctype_alpha($Tel)) {
            $aviso .= "\\nTel";
            $erro = true;
        }
        if ($Cpf == '' || ctype_alpha($Cpf)) {
            //if (preg_match("/^[0-9]{3}\.[0-9]{3}\.[0-9]{3}-[0-9]{2}$/", $Cpf) || $Cpf == ''){
            $aviso .= "\\nCpf";
            $erro = true;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Nascimento)) {
            $aviso .= "\\nNascimento";
            $erro = true;
        }
        if ($Sexo != 'm' && $Sexo != 'f') {
            $aviso .= "\\nSexo";
            $erro = true;
        }

        if ($erro == true) {
            $aviso .= ".";
            echo $aviso;
            $_SESSION['admin']['erro'] = $aviso;
            header('location: index.php');
            exit();
        } else {
            $aviso = '';
        }


        $pdo = conecta();
        if ($Senha == ''){
            $sql = $pdo->prepare("UPDATE cliente SET nome='$Nome', email='$Email', tel='$Tel', cpf='$Cpf', nascimento='$Nascimento', sexo='$Sexo' WHERE id=$Id");
        } else {
            //$SenhaH = password_hash($Senha, PASSWORD_DEFAULT);
            $sql = $pdo->prepare("UPDATE cliente SET nome='$Nome', email='$Email', senha='$Senha', tel='$Tel', cpf='$Cpf', nascimento='$Nascimento', sexo='$Sexo' WHERE id=$Id");
        }
        $sql->execute();
        header('location: index.php');
        ?>
    </body>
</html>
