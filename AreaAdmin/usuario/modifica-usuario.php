<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        //if (!isset($_POST)) {
        //    echo 'nao tem post';
        //    //header('Location: http://www.google.com/');
        // }
        if (!isset($_REQUEST['id_usu'])) {
            header('Location: ../usuario/');
        }
//        if (!isset($_SESSION['admin'])) {
        //          header('location: ../home');
        //        exit();
        //  } else {
        $id_usu = $_REQUEST['id_usu'];
        include '../arquivos/estrutura/header.php';

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>
        <script type="text/javascript" >
            function show(toBlock) {
                setDisplay(toBlock, 'block');
            }
            function hide(toNone) {
                setDisplay(toNone, 'none');
            }
            function setDisplay(target, str) {
                document.getElementById(target).style.display = str;
            }


            function Mascara(o, f) {
                v_obj = o;
                v_fun = f;
                setTimeout("execmascara()", 1);
            }
            //Função que Executa os objetos
            function execmascara() {
                v_obj.value = v_fun(v_obj.value);
            }
            //Função que padroniza telefone (11) 4184-1241
            function Telefone(v) {
                v = v.replace(/\D/g, "").replace(/^(\d\d)(\d)/g, "($1) $2").replace(/(\d{4})(\d)/, "$1-$2");
                return v;
            }
            //Função que padroniza CPF
            function Cpf(v) {
                v = v.replace(/\D/g, "").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d)/, "$1.$2").replace(/(\d{3})(\d{1,2})$/, "$1-$2");
                return v;
            }

            $(document).ready(function () {

                // valida todos os campos ao deixar o foco deles
                $('#nome, #email, #tel, #cpf, #senha, #nascimento').blur(function () {
                    validarCampo($(this));
                });

                /* VALIDAÇÃO ELEMENTOS */
                $('#user').submit(function (event) {

                    var alerta = $('.alert');
                    var alertaTexto = "";

                    // valida o campo nome
                    if ($('#nome').val() == "") {
                        alertaTexto += "Campo nome deve ser preenchido.<br>";
                    }

                    // valida o campo email
                    if ($('#email').val() == "") {
                        alertaTexto += "Campo email deve ser preenchido.<br>";
                    } else {
                        var regexEmail = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
                        if (!regexEmail.test($('#email').val())) {
                            alertaTexto += "Campo email deve ser um email válido.<br>";
                        }
                    }

                    // valida o campo telefone
                    if ($('#tel').val() == "") {
                        alertaTexto += "Campo telefone deve ser preenchido.<br>";
                    }



                    // valida o campo CPF
                    if ($('#cpf').val() == "") {
                        alertaTexto += "Campo CPF deve ser preenchido.<br>";
                    }
                    // valida o campo nascimento
                    if ($('#nascimento').val() == "") {
                        alertaTexto += "Campo nascimento deve ser preenchido.<br>";
                    }

                    // SE EXISTIR ERRO NA VALIDAÇÃO MOSTRA A MENSAGEM DE ERRO
                    if (alertaTexto != "") {
                        alerta.html(alertaTexto);
                        alerta.show();
                        event.preventDefault(); // previne o formulário de ser submetido
                    } else {
                        //alert('Formulário OK!'); // validação ok, todos os campos estão preenchidos
                    }

                });
            });
        </script>

    </head>
    <body>
        <?php
        $pdo = conecta();
        $sql = $pdo->prepare("Select * FROM cliente WHERE id = '$id_usu'");
        $sql->execute();
        while ($linha = $sql->fetch(PDO::FETCH_ASSOC)) {

            echo "
        <div class='container jumbotron'>
        <h2 class='text-center'>Cadastro de Usuário</h2>
        <div class='alert alert-danger'></div>
        <hr>
        <form class='form-horizontal' action='att-usuario.php' method='POST' id='user'>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Nome</label>
                <div class='col-sm-9'>
                    <input name='nome' type='text' id='nome' maxlength='30' class='form-control' value='";
            echo $linha['nome'];
            echo "' autofocus>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Email</label>
                <div class='col-sm-9'>
                    <input name='email' type='email' id='email' maxlength='30' class='form-control' value='";
            echo $linha['email'];
            echo "'>
                </div>
            </div>
<div class='form-group'>";
            ?>
            <label class='col-sm-3 control-label'>Senha</label>
            <div class='col-sm-9'>
                <!--<input name='senha' type='password' id='senha' maxlength='30' class='form-control' value='$row[6]'>-->
                <div class='btn btn-info' id='alt_se'
                     onclick="show('senha');
                             hide('alt_se')">Alterar Senha</div>
                <?php
                echo "
<input name='senha' type='password' id='senha' maxlength='30' class='form-control' placeholder='Alterar senha' style='display:none;'>
                    </div></div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Telefone</label>
                <div class='col-sm-9'>
                    <input name='tel' type='text' id='tel' maxlength='18' onKeyDown='Mascara(this, Telefone);
                           ' onKeyPress='Mascara(this, Telefone);
                           ' onKeyUp='Mascara(this, Telefone);'  class='form-control' value='";
                echo $linha['tel'];
                echo "'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>CPF</label>
                <div class='col-sm-9'>
                    <input maxlength='14' onKeyDown='Mascara(this, Cpf);
                           ' onKeyPress='Mascara(this, Cpf);
                           ' onKeyUp='Mascara(this, Cpf);' name='cpf' type='text' id='cpf' class='form-control' value='";
                echo $linha['cpf'];
                echo "'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Nascimento</label>
                <div class='col-sm-9'>
                    <input  name='nascimento' type='date' id='nascimento' class='form-control' value='";
                echo $linha['nascimento'];
                echo "'>
                </div>
            </div>
            <div class='form-group'>
                <label class='col-sm-3 control-label'>Sexo</label>
                <div class='col-sm-9'>
                    <div class='input-group-addon' style='background-color:transparent;'> ";
                if ($linha['sexo'] == 'f') {
                    $masc = "";
                    $femin = "checked";
                } else {
                    $masc = "checked";
                    $femin = "";
                }
                echo
                "<input name='sexo' type='radio' id='f' value='f' $femin/> <label for='f' style='margin-right:20px'>Feminino</label>
                        <input name='sexo' type='radio' id='m' value='m' $masc/> <label for='m' >Masculino</label>
                    </div>
                </div>
            </div>
            <input type = 'hidden' name = 'id_usu' value = '$id_usu'>
            <div class='nav'>
                <button type='submit' class='btn btn-danger' onclick='javascript:history.back()'>Cancelar</button>
                <button type='submit' class='btn btn-success'>Enviar</button>
                </form>
            </div>
            ";
            }

            include '../arquivos/estrutura/footer.php';

