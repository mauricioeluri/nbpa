<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        session_start();

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>
    </head>
    <body>
        <?php
        $Nome = $_POST['nome'];
        $Genero = $_POST['genero'];
        $Raca = $_POST['raca'];
        $Pelo = $_POST['pelo'];
        $Nascimento = $_POST['nascimento'];
        $Previsao_adocao = $_POST['previsao_adocao'];
        $Descricao = $_POST['descricao'];
        $Nro_img = $_POST['nro_img'];




        $img[0] = '';
        $img[1] = '';
        $img[2] = '';
        $img[3] = '';
        $img[4] = '';

        $x = 0;
        while ($x < $Nro_img) {

            $target_dir = "../../arquivos/img/animal/";
            $ani_img = $_FILES['fileToUpload' . $x]['name'];
            $ani_img_n = $_FILES['fileToUpload' . $x]['name'];
            $ani_img_s = $_FILES['fileToUpload' . $x]['size'];
            $ani_img_t = $_FILES['fileToUpload' . $x]['tmp_name'];
            if (isset($ani_img)) {


                $target_file = $target_dir . basename($ani_img_n);
                $uploadOk = 1;
                $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
                $ext = end((explode(".", $target_file))); # extra () to prevent notice
                $nomeArq = uniqid() . '.' . $ext;
                $target_dir .= $nomeArq;
// Checar se o arquivo de imagem e verdadeiro
                if (isset($_POST["submit"])) {
                    $check = getimagesize($ani_img_t);
                    if ($check !== false) {
                        //echo "<br>O arquivo é uma imagem - " . $check["mime"] . ".";
                        $uploadOk = 1;
                    } else {
                        //echo "<br>O arquivo não é uma imagem.";
                        $uploadOk = 0;
                    }
                }
// Check if file already exists
//if (file_exists($target_file)) {
//    echo "<br>O arquivo já existe.";
//    $uploadOk = 0;
//}
// Check file size
                /* if ($ani_img_s > 500000) {
                  //echo "<br>O tamanho do arquivo é muito grande.";
                  $uploadOk = 0;
                  } */
// Allow certain file formats
                if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                    //echo "<br>Somente arquivos JPG, JPEG, PNG & GIF são permitidos.";
                    $uploadOk = 0;
                }
// Check if $uploadOk is set to 0 by an error
                if ($uploadOk == 0) {
                    //echo "<br>Seu arquivo não foi enviado.";
                    $nomeArq = '';
                    $ani_img = '';
                    $ani_img_n = '';
                    $ani_img_s = '';
                    $ani_img_t = '';

// if everything is ok, try to upload file
                } else {
                    if (move_uploaded_file($ani_img_t, $target_dir)) {
                        chmod($target_dir, 0777);
                        //echo "<br>O arquivo " . basename($_FILES["fileToUpload"]["name"]) . " foi enviado com sucesso.<br>$nomeArq";
                    } else {
                        //echo "<br>Desculpe, houve um erro ao enviar o seu arquivo.";
                    }
                }
            }
            $img[$x] = $nomeArq;
            echo $img[$x];
            echo '<br>';
            $x++;
        }


        $aviso = "Erro!!\\nPor favor verifique os seguintes campos no formulário:";
        $erro = false;
        if ($Nome == '') {
            $aviso .= "\\nNome";
            $erro = true;
        }
        if ($Genero != 'm' && $Genero != 'f') {
            $aviso .= "\\nGênero";
            $erro = true;
        }
        if ($Raca == '') {
            $aviso .= "\\nRaça";
            $erro = true;
        }
        if ($Pelo == '') {
            $aviso .= "\\nPêlo";
            $erro = true;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Nascimento)) {
            $aviso .= "\\nNascimento";
            $erro = true;
        }
        //VALIDAÇÃO DA DATA
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Previsao_adocao)) {
            $aviso .= "\\nPrevisão de adoção";
            $erro = true;
        }

        if ($erro == true) {
            $aviso .= ".";
            echo $aviso;
            $_SESSION['admin']['erro'] = $aviso;
            header('location: index.php');
            exit();
        } else {
            $aviso = '';
        }



        $pdo = conecta();
        $gravaanimal = $pdo->prepare("INSERT INTO animal (nome, genero, raca, pelo, nascimento, previsao_adocao, descricao,img1,img2,img3,img4,img5) VALUES ('$Nome', '$Genero', '$Raca', '$Pelo','$Nascimento','$Previsao_adocao', '$Descricao', '$img[0]', '$img[1]', '$img[2]', '$img[3]', '$img[4]')");
        $gravaanimal->execute();
        header('location: index.php');
        ?>
    </body>
</html>
