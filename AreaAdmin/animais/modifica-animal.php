<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        //if (!isset($_POST)) {
        //    echo 'nao tem post';
        //    //header('Location: http://www.google.com/');
        // }
        if (!isset($_REQUEST['id_anim'])) {
            header('Location: ../animais/');
        }
//        if (!isset($_SESSION['admin'])) {
        //          header('location: ../home');
        //        exit();
        //  } else {
        $id_anim = $_REQUEST['id_anim'];
        include '../arquivos/estrutura/header.php';

        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        $pdo = conecta();
        $modificaanimal = $pdo->prepare("Select * FROM animal WHERE id = '$id_anim'");
        $modificaanimal->execute();



        while ($linha = $modificaanimal->fetch(PDO::FETCH_ASSOC)) {

            echo "

          <div class = 'container jumbotron'>

          <h2 class = 'text-center'>Atualizar Animal</h2>
          <div class = 'alert alert-danger'></div>
          <hr>
          <form class = 'form-horizontal' method = 'POST' action = 'att-animal.php'>
          <div class = 'form-group'>
          <label class = 'col-sm-3 control-label'>Nome</label>
          <div class = 'col-sm-9'>
          <input name = 'nome' type = 'text' id = 'nome' maxlength = '30' class = 'form-control' autofocus value = '";
            echo $linha['nome'];
            echo "'>
          </div>
          </div>
          <div class = 'form-group'>
          <label class = 'col-sm-3 control-label'>Genero</label>
          <div class = 'col-sm-9'>
          <div class = 'input-group-addon' style = 'background-color:transparent;'> ";
            if ($linha['genero'] == 'f') {
                $masc = "";
                $femin = "checked";
            } else {
                $masc = "checked";
                $femin = "";
            }
            echo
            "<input name = 'genero' type = 'radio' id = 'f' value = 'f' $femin/> <label for = 'f' style = 'margin-right:20px'>Feminino</label>
        <input name = 'genero' type = 'radio' id = 'm' value = 'm' $masc/> <label for = 'm' >Masculino</label>
        </div>
        </div>
        </div>
        <div class = 'form-group'>
        <label class = 'col-sm-3 control-label'>Raça</label>
        <div class = 'col-sm-9'>
        <input name = 'raca' type = 'text' id = 'raca' maxlength = '30' class = 'form-control' autofocus value = '";
            echo $linha['raca'];
            echo "'>
        </div> </div>


        <div class = 'form-group'>
        <label class = 'col-sm-3 control-label'>Pelo</label>
        <div class = 'col-sm-9'>
        <input name = 'pelo' type = 'text' id = 'pelo' maxlength = '30' class = 'form-control' autofocus value = '";
            echo $linha['pelo'];
            echo "'>
        </div> </div>
        <div class = 'form-group'>
        <label class = 'col-sm-3 control-label'>Nascimento</label>
        <div class = 'col-sm-9'>
        <input name = 'nascimento' type = 'date' id = 'nascimento' class = 'form-control' value = '";
            echo $linha['nascimento'];
            echo "'>
        </div>
        </div>
        <div class = 'form-group'>
        <label class = 'col-sm-3 control-label'>Previsão de Adoção</label>
        <div class = 'col-sm-9'>
        <input name = 'previsao_adocao' type = 'date' id = 'previsao_adocao' class = 'form-control' value = '";
            echo $linha['previsao_adocao'];
            echo "'>
        </div>
        </div>
        <hr>
        <div class = 'form-group'>
        <textarea rows = '4' style = 'width:400px;' name = 'descricao' id = 'descricao' maxlength = '30' placeholder='Descrição'>";
            echo $linha['descricao'];
            echo "</textarea>
        </div>
        <input type = 'hidden' name = 'id_anim' value = '$id_anim'>
        <div class='nav'>
            <button type='submit' class='btn btn-danger' onclick='javascript:history.back()'>Cancelar</button>
            <button type = 'submit' class = 'btn btn-success' name = 'atualizar'>Atualizar</button>
        </div>
        </form>
        </div>";
        }

        include '../arquivos/estrutura/footer.php';
