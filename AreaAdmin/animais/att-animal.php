<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
        session_start();
        if (!file_exists('../funcoes.php')) {
            include '../arquivos/funcoes.php';
        } else {
            include '../funcoes.php';
        }
        ?>
    </head>
    <body>
        <?php
        $Id = ($_REQUEST['id_anim']);


        $Nome = $_POST['nome'];
        $Genero = $_POST['genero'];
        $Raca = $_POST['raca'];
        $Pelo = $_POST['pelo'];
        $Nascimento = $_POST['nascimento'];
        $Previsao_adocao = $_POST['previsao_adocao'];
        $Descricao = $_POST['descricao'];


        $aviso = "Erro!!\\nPor favor verifique os seguintes campos no formulário:";
        $erro = false;
        if ($Nome == '') {
            $aviso .= "\\nNome";
            $erro = true;
        }
        if ($Genero != 'm' && $Genero != 'f') {
            $aviso .= "\\nGênero";
            $erro = true;
        }
        if ($Raca == '') {
            $aviso .= "\\nRaça";
            $erro = true;
        }
        if ($Pelo == '') {
            $aviso .= "\\nPêlo";
            $erro = true;
        }
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Nascimento)) {
            $aviso .= "\\nNascimento";
            $erro = true;
        }
        //VALIDAÇÃO DA DATA
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $Previsao_adocao)) {
            $aviso .= "\\nPrevisão de adoção";
            $erro = true;
        }

        if ($erro == true) {
            $aviso .= ".";
            echo $aviso;
            $_SESSION['admin']['erro'] = $aviso;
            header('location: index.php');
            exit();
        } else {
            $aviso = '';
        }
        $pdo = conecta();
        $attanimal = $pdo->prepare("UPDATE animal SET nome='$Nome', genero='$Genero', raca='$Raca', pelo='$Pelo', nascimento='$Nascimento', previsao_adocao='$Previsao_adocao', descricao='$Descricao' WHERE id=$Id");
        $attanimal->execute();

        header('location: index.php');
        ?>
    </body>
</html>
