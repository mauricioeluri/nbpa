<?php
session_start();
?>
<link rel=icon type=image/png href=../arquivos/img/logo2.png>
<meta charset="UTF-8">
<meta name=description content="NBPA BAGE">
<meta name=viewport content="width=device-width, initial-scale=1">
<link href='../arquivos/css/bootstrap.min.css' rel='stylesheet' type='text/css'/>
<script src='../arquivos/js/jquery-2.1.4.min.js' type='text/javascript'></script>
<script>
//AJUSTES PARA VISUALIZAÇÃO CORRETA DO MENU
    $(document).ready(function () {
        var path = document.location.pathname;
        var url = path.substring(path.indexOf('/', 1) + 1, path.lastIndexOf('/'));
        function add() {
            $("p").addClass("menu3");
            $("ul").addClass("menu2");
            $(".navbar-default").addClass("menu1");
            $(".navbar-default").addClass("navbar-static-top");
            $(".navbar-default").removeClass("navbar-fixed-top");
        }
        function fixed() {
            $("p").removeClass("menu3");
            $("ul").removeClass("menu2");
            $(".navbar-default").removeClass("menu1");
            $(".navbar-default").addClass("navbar-fixed-top");
            $(".navbar-default").removeClass("navbar-static-top");
        }
        function testeMenu() {
            if (($(window).width()) > 752) {
                //console.log("Scrolled in desktop version");
                add();
            }
            else {
                //console.log("Scrolled in mobile version");
                fixed();
            }
        }
        function trocaMenu() {
            var height = $(window).scrollTop();
            if (height > 51) {
                fixed();
                //console.log(height);
            }
            if (height < 51 && ($(window).width()) > 752) {
                add();
                //console.log("Trocou para add");
            }
        }
        if (url === "/" || url === "nbpa/home") {
            $(window).ready(function () {
                testeMenu();
                trocaMenu();

            });
            $(window).on("resize scroll", function () {
                testeMenu();
                trocaMenu();
            });

        }
    });
</script>
<link href='../arquivos/css/normal/estrutura.css' rel='stylesheet' type='text/css'/>
<!--[if lt IE 9]>
    <script>
alert('Você está usando um navegador antigo!\n\nAlguns itens podem não aparecer corretamente.\nConsidere atualizar para uma versão mais recente.');
</script>
<![endif]-->
<script src='../arquivos/js/normal/main.js' type='text/javascript'></script>
<link rel="stylesheet" href="../arquivos/css/superslides.css">
<script src="../arquivos/js/jquery.superslides.min.js" type="text/javascript"></script>
<style>
    .header{
        background: black;
        width: 100%;
        height: 50px;
        position: fixed;
        display: none;
    }
</style>
</head>
<body>
    <div id="esqueci" style="display:none;">
        <div class="escuro" onclick="hide('esqueci');"></div>
        <form action="../enviar.php" method="POST">
            <div class="jumbotron env_mail">
                <h3 style="color: white;">Esqueceu a senha?</h3>
                <hr style=" margin: 0;">
                <p class="texto_senha">Escreva o endereço de email da sua conta abaixo:</p>
                <input type="text" class="form-control" style="font-size: 18px" name="email" autofocus/>
                <hr class="hr1">

                <p class="p2" onclick="hide('esqueci');" >cancelar</p>
                <button type='submit' class='btn btn-success center-block' name='value' id='value' style="float: right" onclick='show_alert();'>Enviar e-mail!</button>
            </div>
        </form>
    </div>
    <?php
    $url = $_SERVER['SCRIPT_NAME'];
    if ($url == "/npba/nbpa/site/home/index.php") {
        $menu1 = "menu1 navbar-static-top";
        $menu2 = "menu2";
        $menu3 = "menu3";
    } else {
        $menu1 = "navbar-fixed-top";
        $menu2 = "";
        $menu3 = "";
    }
    echo "
    <div class='header'></div>
    <nav class='navbar navbar-default $menu1'>
        <div id='topoTudo'>
            <div class='navbar-header'>
                <button type='button' class='navbar-toggle' data-toggle='collapse' data-target='#navbar-collapse-1'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                </button>
                <a id='log' href='../home'>
                    <p class='navbar-brand $menu3'>NBPA Bagé</p></a>
            </div>
            <div class='collapse navbar-collapse' id='navbar-collapse-1'>
                <div id='topoItem'>
                    <ul class='nav navbar-nav $menu2'>
                        <li><a href='../home' class='link menutransp'>Home</a></li>
                        <li><a href='../animais' class='link menutransp'>Animais</a></li>
                        <li><a href='../servicos' class='link menutransp'>Serviços</a></li>
            ";
    if (!isset($_SESSION['usuario'])) {
        echo "
<li class='dropdown'>
    <a href='#' class='dropdown-toggle' data-toggle='dropdown'>Login <b class='caret'></b></a>
    <ul class='dropdown-menu menutransp'>
        <li style='padding: 15px; padding-bottom: 0px;'>
            <form method='post' action='../login.php' accept-charset='UTF-8'>
                <input style='margin-bottom: 15px;' type='text' placeholder='E-mail' name='login'>
                <input style='margin-bottom: 15px;' type='password' placeholder='Senha' name='senha'>"
        ?>
        <div onclick="show('esqueci');"><label class='string optional'>  * Esqueci minha senha </label></div>
        <?php
        echo"<input class='btn btn-primary btn-block' type='submit' id='sign-in' value='Entrar'>
            </form>
            <form action='../cadastre'>
                <input class='btn btn-success btn-block' type='submit' style='margin-top:5px;' value='Cadastre-se'>
            </form>
        </li>
    </ul>
</li> ";
    } else {
        echo " <ul class='nav navbar-nav navbar-right'>
                             <li class='dropdown'>
                                 <a href='#' class='dropdown-toggle' data-toggle='dropdown'>
                                     <i class='fa fa-user'></i>
                                     <strong>";
        echo $_SESSION['usuario']['usuario'];
        echo "</strong>
                                     <i class='fa fa-sort-desc'></i>
                                 </a>
                                 <ul class='dropdown-menu'>
                                     <li>
                                         <div class='navbar-login'>
                                             <div class='row'>
                                                 <div class='col-lg-4'>
                                                     <p class='text-center'>
                                                         <i class='fa fa-user-secret fa-4x'></i>
                                                     </p>
                                                 </div>
                                                 <div class='col-lg-8'>
                                                     <p>
                                                         <a href='../arquivos/alt_dados.php' class='btn btn-primary btn-block btn-sm semborda'>Atualizar dados</a>
                                                         <a href='../logout.php' class='btn btn-danger btn-block semborda'>Sair</a>
                                                     </p>

                                                 </div>
                                             </div>
                                         </div>
                                     </li>
                                 </ul>
                             </li>
                        </ul>";
    }
    ?>

</ul>
</div>
</div>
</div>
</nav>
<div id="content">
