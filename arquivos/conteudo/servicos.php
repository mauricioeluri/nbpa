<div style='margin-top: 40px'></div>
<?php
if (!file_exists('../funcoes.php')) {
    include '../arquivos/funcoes.php';
} else {
    include '../funcoes.php';
}
?>
<div class="container-fluid" style="background-color: white;">
    <div class="col-md-12">
        <h2 style="margin-top:50px">QUEM NÓS SOMOS</h2>
        <div style="margin-top:45px"></div>
        <div class="col-md-5">
            <img src="../arquivos/img/caoegarota.jpg" class="respo">
        </div>
        <div class="col-md-7">
            <h3 style="margin-top: 0;text-align: left;">Título do texto</h3>
            <p style="    font-size: 19px;color: #4C4C4C; text-align: justify">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient ontes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante.</p>
        </div>
    </div>
    <div>
        <div class="row" >
            <hr><br>
            <div class="col-lg-12 divimg respo">
                <h2 class="h2img">SEMPRE CUIDANDO</h2>

            </div>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <h2 style="margin-top:30px">O QUE FAZEMOS</h2>
        <div class="col-md-6" style="padding:55px;">
            <div class="col-md-12">
                <h3 style="float:left"><i class="fa fa-bullhorn fa-2x"></i> Lorem Ipsum</h3>
            </div>
            <div class="col-md-12">
                <p style="font-size: 19px;color: #4C4C4C; text-align: justify">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient ontes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante.</p>
            </div>
        </div>

        <div class="col-md-6" style="padding:55px;">
            <div class="col-md-12">
                <h3 style="float:left"><i class="fa fa-paw fa-2x"></i> Lorem Ipsum</h3>
            </div>
            <div class="col-md-12">
                <p style="font-size: 19px;color: #4C4C4C; text-align: justify">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient ontes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante.</p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="col-md-6" style="padding:55px;">
            <div class="col-md-12">
                <h3 style="float:left"><i class="fa fa-trophy fa-2x"></i> Lorem Ipsum</h3>
            </div>
            <div class="col-md-12">
                <p style="font-size: 19px;color: #4C4C4C; text-align: justify">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient ontes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante.</p>
            </div>
        </div>

        <div class="col-md-6" style="padding:55px;">
            <div class="col-md-12">
                <h3 style="float:left"><i class="fa fa-heart fa-2x"></i> Lorem Ipsum</h3>
            </div>
            <div class="col-md-12">
                <p style="font-size: 19px;color: #4C4C4C; text-align: justify">Aenean nonummy hendrerit mauris. Phasellus porta. Fusce suscipit varius mi. Cum sociis natoque penatibus et magnis dis parturient ontes, nascetur ridiculus mus. Nulla dui. Fusce feugiat malesuada odio. Morbi nunc odio, gravida at, cursus nec, luctus a, lorem. Maecenas tristique orci ac sem. Duis ultricies pharetra magna. Donec accumsan malesuada orci. Donec sit amet eros. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Mauris fermentum dictum magna. Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante.</p>
            </div>
        </div>
    </div>
</div>