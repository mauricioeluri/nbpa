<div style='margin-top: 40px'></div>
<?php
if (!file_exists('../funcoes.php')) {
    include '../arquivos/funcoes.php';
} else {
    include '../funcoes.php';
}

$pdo = conecta();
$sql = $pdo->prepare("SELECT id, nome, genero, raca, pelo, nascimento, previsao_adocao, descricao, img1, img2, img3, img4, img5 FROM animal order by id desc;");
$sql->execute();

echo "<script type='text/javascript'> var login = false;";
if (!isset($_SESSION['usuario']['login'])) {
    echo "var login = false;";
} else {
    echo "var login = true;";
}
?>
function testLogin() {
if (login == false) {
alert("Você precisa estar logado para poder prosseguir.");
event.preventDefault();
} else {
onclick: location.href = '../adocao';
}
}
</script>
<div class="col-md-12 adote">
    <form action="../adocao/index.php" method="post" id="cadastra">

        <?php
        while ($linha = $sql->fetch(PDO::FETCH_ASSOC)) {
            echo "
        <div class='container jumbotron'>
            <h2 class='nome_user'>";
            echo $linha['nome'];
            echo "</h2>
            <hr>
            <div class='form-group col-sm-12'>
            <div>";

            $x = 1;
            //$linha['img'.$x];
            while ($x < 5) {
                if ($linha['img' . $x] !== '' && $linha['img' . $x] !== NULL) {
                    echo "<div class='col-sm-12' style='margin-top:10px; margin-bottom:20px;'><img src='../../site/arquivos/img/animal/";
                    echo $linha['img' . $x];
                    echo"' style='width:100%;'></div>";
                }
                $x++;
            }

            echo "</div>

                <form class='form-horizontal' action='../adocao/index.php' method='POST' id='contactForm'>
                    <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Idade: ";
            $dob = $linha['nascimento'];
            echo ageCalculator($dob);
            if (ageCalculator($dob) == 1) {
                echo " ano";
            } else {
                echo " anos";
            }

            echo "</p>
                    </div>
                    <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Raça: ";
            echo $linha['raca'];
            echo "</p></div>
                    <div class='div_50' style='float:left'><p class='dados_user2 dados_user1'>Gênero: ";
            if ($linha['genero'] == 'm') {
                $sexo = 'Masculino';
            } else {
                $sexo = 'Feminino';
            }
            echo "$sexo </p>
                    </div>
                    <div style='float:left'><p class='dados_user2 dados_user1'>Pelo: ";
            echo $linha['pelo'];
            echo "</p></div>
                </div>
                <hr>
                <p class='dados_user2 dados_user1'>Descrição: ";
            if ($linha['descricao'] == '' || $linha['descricao'] == NULL) {
                $Descricao = 'Sem descrição';
            } else {
                $Descricao = $linha['descricao'];
            }
            echo "$Descricao</p>
                <input type='hidden' name='id_clie' id='id_clie' value='";
            echo $linha['id'];
            echo "'/>
                <button type='submit' class='btn btn-success center-block' onclick='testLogin();' name='value' id='value' value='";
            echo $linha['id'];
            echo "' id='contactForm'>Adotar!</button>
            </form>
        </div>
";
        }
        ?>
    </form>
</div>