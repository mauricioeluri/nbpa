<style>
    .btn-file {
        position: relative;
        overflow: hidden;
    }
    .btn-file input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        min-width: 100%;
        min-height: 100%;
        font-size: 100px;
        text-align: right;
        filter: alpha(opacity=0);
        opacity: 0;
        background: red;
        cursor: inherit;
        display: block;
    }
    input[readonly] {
        background-color: white !important;
        cursor: text !important;
    }
</style>
<div style='margin-top: 120px'></div>
<div class="container jumbotron">
    <h2 class="text-center">Cadastro de Usuário</h2>
    <div class="alert alert-danger"></div>
    <hr>
    <form class="form-horizontal" action="grava-usuario.php" method="POST" id="contactForm" enctype="multipart/form-data">
        <div class="form-group">
            <label class="col-sm-3 control-label">Foto de usuário</label>
            <!-- <div class="col-sm-9"> -->
                <!-- <input type="file" name="fileToUpload" > -->

            <div class="col-sm-9">
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">
                            Browse&hellip; <input name="fileToUpload" type="file" accept="image/*" >
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly>
                </div>
            </div>
            <!-- </div> -->
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nome</label>
            <div class="col-sm-9">
                <input
                    name="nome" type="text" id="nome" maxlength="30" class="form-control" autofocus>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Email</label>
            <div class="col-sm-9">
                <input name="email" type="email" id="email" maxlength="30" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Telefone</label>
            <div class="col-sm-9">
                <input name="tel" type="text" id="tel" maxlength="18" onKeyDown="Mascara(this, Telefone);" onKeyPress="Mascara(this, Telefone);" onKeyUp="Mascara(this, Telefone);"  class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">CPF</label>
            <div class="col-sm-9">
                <input maxlength="14" onKeyDown="Mascara(this, Cpf);" onKeyPress="Mascara(this, Cpf);" onKeyUp="Mascara(this, Cpf)
                                ;" name="cpf" type="text" id="cpf" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Senha</label>
            <div class="col-sm-9">
                <input name="senha" type="password" id="senha" maxlength="30" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Nascimento</label>
            <div class="col-sm-9">
                <input  name="nascimento" type="date" id="nascimento" class="form-control">
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label">Sexo</label>
            <div class="col-sm-9">
                <div class="input-group-addon" style="background-color:transparent;">
                    <input name="sexo" type="radio" id="f" value="f" checked/> <label for="f" style="margin-right:20px">Feminino</label>
                    <input name="sexo" type="radio" id="m" value="m"/> <label for="m" >Masculino</label>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-success center-block" id="contactForm">Enviar</button>
    </form>
</div>