



//ENVIAR EMAIL
function show_alert() {
    document.onload = setTimeout(function () {
        alert("E-mail enviado com sucesso!");
    }, 5000);
}
//ESCONDER E MOSTRAR ITENS
function show(toBlock) {
    setDisplay(toBlock, 'block');
}
function hide(toNone) {
    setDisplay(toNone, 'none');
}
function setDisplay(target, str) {
    document.getElementById(target).style.display = str;
}


// PASSAR SLIDES HOME
$(document).ready(function () {
    $('#slides').superslides({
        hashchange: true,
        play: 5000,
        animation: 'fade'
    });
    /* $('#slides').on('mouseenter', function () {
        $(this).superslides('stop');
    });
    $('#slides').on('mouseleave', function () {
        $(this).superslides('start');
    }); */
});
//BOTAO AREA ADMIN MANDA PRA OUTRA JANELA
function areaAdmin() {
    onclick: location.href = '../AreaAdmin';
    OpenInNewTab('../AreaAdmin');
}
function OpenInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
//ESSA E FACIL...
function Logout() {
    onclick: location.href = '../logout.php';
}
// FECHAR MENU RESPONSIVO QUANDO CLICADO
$(document).ready(function () {
    $(".link").click(function () {
        $(".navbar-collapse").collapse('hide');
    });
});
/* ALTERAR LOGO HOVER */
var sourceSwap = function () {
    var $this = $('.logo');
    var newSource = $this.data('alt-src');
    $this.data('alt-src', $this.attr('src'));
    $this.attr('src', newSource);
};